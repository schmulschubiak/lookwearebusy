#!/bin/bash
printf "### LookWeAreBusy: system setup and install latest version\n"

printf "\n### LookWeAreBusy: system update\n"
apt-get update

printf "\n### LookWeAreBusy: system upgrade\n"
apt-get upgrade -y

printf "\n### LookWeAreBusy: install dependencies\n"
# X-Server
apt install -y --no-install-recommends xserver-xorg-video-all xserver-xorg-input-all xserver-xorg-core xinit x11-xserver-utils
# Hide cursor
apt install -y unclutter
# Tauri AppImage dependencies
apt install -y fuse
apt install -y libharfbuzz-bin
apt install -y libfribidi0
apt install -y libthai0
apt install -y libgles2

printf "\n### LookWeAreBusy: disable wifi and bluetooth, Set core speed for SPI\n"
# Append in 11th line of /boot/firmware/config.txt
# Core speed for SPI is set to 500MHz, to keep the LED stable while throttling
sed -i '11a\\n# Disable WiFi and Bluetooth, set core speed for SPI\ndtoverlay=disable-wifi\ndtoverlay=disable-bt\ncore_freq=500\ncore_freq_min=500\ngpu_mem=512\n' /boot/firmware/config.txt

printf "\n### LookWeAreBusy: set console autologin\n"
raspi-config nonint do_boot_behaviour B2

printf "\n### LookWeAreBusy: enable SPI\n"
# 0 = enable, 1 = disable
raspi-config nonint do_spi 0

printf "\n### LookWeAreBusy: hide all boot messages\n"
# redirect console messages to tty3
sed -i s/console=tty1/console=tty3/g /boot/firmware/cmdline.txt
# disable cursor, set loglevel to 3, disable splash, disable logo
sed -i s/$/' quiet disable_splash=1 loglevel=3 logo.nologo vt.global_cursor_default=0'/g /boot/firmware/cmdline.txt
# disable login message
sed -i '3cExecStart=-/sbin/agetty --skip-login --noclear --noissue --login-options "-f lookwearebusy" %I $TERM' /etc/systemd/system/getty@tty1.service.d/autologin.conf
touch /home/lookwearebusy/.hushlogin
chown -c lookwearebusy:lookwearebusy /home/lookwearebusy/.hushlogin

printf "\n### LookWeAreBusy: download lookwearebusy main app\n"
# curl latest.json from https://releases.wolkensammler.de/lookwearebusy/latest.json then extract the url and download it via curl
curl -s https://releases.wolkensammler.de/lookwearebusy/latest.json | grep -o 'https.*AppImage' | xargs curl -o /home/lookwearebusy/look-we-are-busy.AppImage
chown -c lookwearebusy:lookwearebusy /home/lookwearebusy/look-we-are-busy.AppImage
chmod a+x /home/lookwearebusy/look-we-are-busy.AppImage

printf "\n### LookWeAreBusy: create start script\n"
# script to start the app and keep it running, because the app update won't start the app itself after restart
cat << EOF > /home/lookwearebusy/lookwearebusy_start.sh
#!/bin/bash
echo "### LookWeAreBusy: start and keep running"
export DISPLAY=:0 #needed if you are running a simple gui app.

app="/home/lookwearebusy/look-we-are-busy.AppImage"

while true; do
  if ps ax | grep -v grep | grep \$app > /dev/null
  then
    echo "\$app is running"
  else
    echo "\$app is not running... restarting..."
    \$app &
  fi
  sleep 1
done
EOF
chown -c lookwearebusy:lookwearebusy /home/lookwearebusy/lookwearebusy_start.sh
chmod a+x /home/lookwearebusy/lookwearebusy_start.sh

printf "\n### LookWeAreBusy: setup X environment\n"
cat << EOF > /home/lookwearebusy/.bash_profile
if [ -z \$DISPLAY ] && [ \$(tty) = /dev/tty1 ]
then
    startx >/dev/null 2>&1
fi
EOF
chown -c lookwearebusy:lookwearebusy /home/lookwearebusy/.bash_profile

cat << EOF > /home/lookwearebusy/.xinitrc
#!/usr/bin/env sh
xset -dpms
xset s off
xset s noblank

unclutter & sh /home/lookwearebusy/lookwearebusy_start.sh
EOF
chown -c lookwearebusy:lookwearebusy /home/lookwearebusy/.xinitrc

printf "\n### LookWeAreBusy: install and setup done. reboot.\n"
reboot