import { App, Plugin } from "vue";
import VLoading from "element-plus/es/components/loading/index";

import {
  useColorMode,
  usePreferredDark,
} from "@vueuse/core";

export function createElementPlus(): Plugin {
  return {
    install(app: App) {
      useColorMode({
        selector: "html",
        attribute: "class",
      }).value = usePreferredDark().value ? "dark" : "light";

      app.use(VLoading);


    },
  } as Plugin;
}
