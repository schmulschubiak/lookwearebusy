import {App, computed, Plugin} from "vue";
import { Composer, createI18n } from "vue-i18n";
import locale_de from "../locales/de.json";
import locale_en from "../locales/en.json";
import { useSessionStore } from "../store/session.ts";
import de from "element-plus/dist/locale/de.mjs";
import en from "element-plus/dist/locale/en.mjs";


type MessageSchema = typeof locale_en;

const createdI18n = createI18n<[MessageSchema], "de" | "en">({
  legacy: false,
  globalInjection: true,
  locale: "en",
  fallbackLocale: "en",
  messages: {
    de: locale_de,
    en: locale_en,
  },
});

export function createVueI18n(language: string): Plugin {
  return {
    install(app: App) {
      app.use(createdI18n);
      i18n.locale.value = language;
    },
  } as Plugin;
}

// @ts-ignore
export const i18n = createdI18n.global as Composer;

export const elementPlusLocale = computed(() => {
  if (useSessionStore().language === "en") {
    return en;
  } else {
    return de;
  }
});