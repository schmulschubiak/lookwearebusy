/// <reference types="vite/client" />

declare module "*.vue" {
  import type { DefineComponent } from "vue";
  const component: DefineComponent<{}, {}, any>;
  export default component;
}

declare module "element-plus/dist/locale/de.mjs" {
  const deLocale: any;
  export default deLocale;
}

declare module "element-plus/dist/locale/en.mjs" {
  const enLocale: any;
  export default enLocale;
}

declare const __APP_VERSION__: string