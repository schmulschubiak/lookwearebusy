import {createVueI18n} from "./plugins/vue-i18n.ts";
import {useSessionStore} from "./store/session.ts";
import {createElementPlus} from "./plugins/element-plus.ts";
import {createPinia} from "pinia";
import {createApp} from "vue";
import App from "./App.vue";
import '@fontsource-variable/tilt-neon';
import "./styles/index.scss";

const vueApp = createApp(App).use(createPinia());

await useSessionStore().loadConfig();

vueApp
.use(createVueI18n(useSessionStore().language))
.use(createElementPlus())
.mount("#app");
