import { defineStore } from "pinia";
import { invoke } from "@tauri-apps/api/tauri";
import { ElMessage } from "element-plus";
import { i18n } from "../plugins/vue-i18n.ts";

export type State = {
  language: string;
  who_is_busy_url: string;
  who_is_busy_user_id_prefix: string;
  who_is_busy_user_ids: string[];
  who_is_busy_last_status: Status;
  app_version: string;
};

export const useSessionStore = defineStore("session", {
  state: (): State => ({
    language: "de",
    who_is_busy_url: "",
    who_is_busy_user_id_prefix: "",
    who_is_busy_user_ids: [],
    who_is_busy_last_status: Status.UNKNOWN,
    app_version: __APP_VERSION__ ?? "0.0.0",
  }),
  actions: {
    async loadConfig() {
      const config: State = await invoke("read_config");
      this.language = config.language;
      this.who_is_busy_url = config.who_is_busy_url;
      this.who_is_busy_user_id_prefix = config.who_is_busy_user_id_prefix;
      this.who_is_busy_user_ids = config.who_is_busy_user_ids;
    },
    async readStatus() {
      const status: string = await invoke("get_status");
      this.who_is_busy_last_status = status.toUpperCase();
    },
    async saveConfig() {
      await invoke("update_config", {
        config: {
          language: this.language,
          who_is_busy_url: this.who_is_busy_url,
          who_is_busy_user_id_prefix: this.who_is_busy_user_id_prefix,
          who_is_busy_last_status: this.who_is_busy_last_status,
          who_is_busy_user_ids: this.who_is_busy_user_ids,
        },
      });

      ElMessage.success({
        message: i18n.t("config.common.save.success"),
        showClose: true,
      });
    },
  },
});

export const Status = {
  AVAILABLE: "AVAILABLE",
  ABSENT: "ABSENT",
  DONOTDISTURB: "DONOTDISTURB",
  OFFLINE: "OFFLINE",
  NEWACTIVITY: "NEWACTIVITY",
  UNKNOWN: "UNKNOWN",
};

export type Status = (typeof Status)[keyof typeof Status];
