use log::debug;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct AppConfig {
    pub language: String,
    pub who_is_busy_url: String,
    pub who_is_busy_user_id_prefix: String,
    pub who_is_busy_user_ids: Vec<String>,
}

impl Default for AppConfig {
    fn default() -> Self {
        Self {
            language: "en".into(),
            who_is_busy_url: "https://whoisbusy.alwaysdata.net".into(),
            who_is_busy_user_id_prefix: "00000000000040".into(),
            who_is_busy_user_ids: vec![],
        }
    }
}

lazy_static::lazy_static! {
    pub static ref CONFIG: std::sync::Mutex<AppConfig> = {
        let config = load().unwrap_or_default();
        std::sync::Mutex::new(config)
    };
}

pub fn load() -> Result<AppConfig, confy::ConfyError> {
    debug!(
        "Loading config from confy, Path {}",
        confy::get_configuration_file_path("org.schmulschubiak.lookwearebusy", None)
            .unwrap()
            .display()
    );
    confy::load("org.schmulschubiak.lookwearebusy", None)
}

pub fn save(config: &AppConfig) -> Result<(), confy::ConfyError> {
    confy::store("org.schmulschubiak.lookwearebusy", None, config)
}
