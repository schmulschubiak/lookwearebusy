#[cfg(target_arch = "aarch64")]
use crate::whoisbusy_connector::Status;
#[cfg(target_arch = "aarch64")]
use rs_ws281x::ControllerBuilder;
#[cfg(target_arch = "aarch64")]
use rs_ws281x::ChannelBuilder;
#[cfg(target_arch = "aarch64")]
use rs_ws281x::StripType;

#[cfg(target_arch = "aarch64")]
pub fn send_status_to_rpi(status: Status) {
    match status {
        Status::DoNotDisturb => {
            set_color([0, 0, 255, 0], 48);
        },
        Status::Available => {
            set_color([0, 255, 0, 0], 48);
        },
        _ => {
            set_color([0, 0, 0, 0], 0);
        }
    }
}

/// # Arguments
///
/// * `color`: [?, B, G, R]
/// * `brightness`: 0 - 255
///
/// returns: ()
#[cfg(target_arch = "aarch64")]
fn set_color(color: [u8; 4], brightness: u8) {
    let mut controller = ControllerBuilder::new()
            .freq(800_000)
            .dma(10)
            .channel(
                0, // Channel Index
                ChannelBuilder::new()
                    .pin(10) // GPIO 10 = SPI0 MOSI
                    .count(44) // Number of LEDs
                    .strip_type(StripType::Ws2812)
                    .brightness(brightness)
                    .build(),
            )
            .build()
            .unwrap();

        let leds = controller.leds_mut(0);

        for led in leds {
            *led = color;
        }

        controller.render().unwrap();
}