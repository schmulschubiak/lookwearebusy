// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use log::debug;
use log::LevelFilter;
use tauri_plugin_log::{Builder as LogPluginBuilder, LogTarget};
use crate::whoisbusy_connector::Status;

mod config;
mod rpi_connector;
mod whoisbusy_connector;

#[tauri::command(rename_all = "snake_case")]
fn read_config() -> config::AppConfig {
    let config_guard = config::CONFIG.lock().unwrap();
    let config = config_guard.clone();
    drop(config_guard);
    config
}

#[tauri::command(rename_all = "snake_case")]
fn get_status() -> Status {
    let status = whoisbusy_connector::watch_group_status();

    #[cfg(target_arch = "aarch64")]
    rpi_connector::send_status_to_rpi(status);
    status
}

#[tauri::command(rename_all = "snake_case")]
fn update_config(config: config::AppConfig) {
    let config_guard = config::CONFIG.lock().unwrap();
    let mut app_config = config_guard.clone();
    app_config.language = config.language.clone();
    app_config.who_is_busy_url = config.who_is_busy_url.clone();
    app_config.who_is_busy_user_ids = config.who_is_busy_user_ids.clone();

    debug!("Saving config: {:?}", app_config);

    config::save(&app_config).unwrap();
    drop(config_guard); // Das Mutex freigeben, bevor es wieder gesperrt wird
    *config::CONFIG.lock().unwrap() = app_config;
}

fn main() {
    // Fixes rendering Problems on Webkit Linux
    // https://github.com/tauri-apps/tauri/issues/5143
    std::env::set_var("WEBKIT_DISABLE_COMPOSITING_MODE", "1");

    tauri::Builder::default()
        .plugin(
            LogPluginBuilder::new()
                .targets([
                    #[cfg(debug_assertions)]
                        LogTarget::Stdout,
                    LogTarget::LogDir,
                ])
                .level_for("reqwest::connect", LevelFilter::Warn)
                .level(LevelFilter::Debug)
                .build(),
        )
        .plugin(tauri_plugin_single_instance::init(|app, argv, cwd| {
            debug!("{}, {argv:?}, {cwd}", app.package_info().name);
        }))
        .invoke_handler(tauri::generate_handler![read_config, get_status, update_config])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
