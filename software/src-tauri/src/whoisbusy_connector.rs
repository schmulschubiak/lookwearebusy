use std::time::Duration;

use log::debug;
use serde::{Deserialize, Serialize};

use crate::config;

#[derive(Clone, Copy, PartialEq, Debug, Deserialize, Serialize)]
pub enum Status {
    Available,
    Absent,
    DoNotDisturb,
    Offline,
    NewActivity,
    Unknown,
}

impl ToString for Status {
    fn to_string(&self) -> String {
        match self {
            Status::Available => "Available".to_string(),
            Status::Absent => "Away".to_string(),
            Status::DoNotDisturb => "DoNotDisturb".to_string(),
            Status::Offline => "Offline".to_string(),
            Status::NewActivity => "NewActivity".to_string(),
            Status::Unknown => "Unknown".to_string(),
        }
    }
}

impl From<&str> for Status {
    fn from(s: &str) -> Self {
        match s {
            "Available" | "AVAILABLE" => Status::Available,
            "Away" | "BeRightBack" | "AWAY" | "BERIGHTBACK" => Status::Absent,
            "Busy" | "DoNotDisturb" | "OnThePhone" | "Presenting" | "InAMeeting" | "BUSY"
            | "DONOTDISTURB" | "ONTHEPHONE" | "PRESENTING" | "INAMEETING" => Status::DoNotDisturb,
            "Offline" | "Keine Verbindung" | "OFFLINE" | "KEINEVERBINDUNG" => Status::Offline,
            "NewActivity" | "NEWACTIVITY" => Status::NewActivity,
            _ => Status::Unknown,
        }
    }
}

pub fn watch_group_status() -> Status {
    let config_guard = config::CONFIG.lock().unwrap();
    let url = config_guard.who_is_busy_url.clone();
    let user_ids = config_guard.who_is_busy_user_ids.clone();
    drop(config_guard);

    let client = reqwest::blocking::Client::new();

    let url = format!(
        "{}/api/v1/users/status?withUserId={}",
        url,
        user_ids.join("&withUserId=")
    );

    match client.get(url).timeout(Duration::from_secs(5)).send() {
        Ok(response) => {
            if response.status().is_success() {
                let body = response.text().unwrap();
                debug!("Received group status: {}", body);
                Status::from(body.as_str())
            } else {
                Status::Unknown
            }
        }
        Err(_e) => Status::Unknown,
    }
}
