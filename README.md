# LookWeAreBusy

This is a group busy light extension for LookImBusy busylight.
Based on RaspberryPi.

## Install

- prepare RaspberryPi with Raspbian OS Lite (64bit)
  - setup user "lookwearebusy"
  - enable ssh
- boot into RaspberryPi
- run install script
  - `curl -o lookwearebusy_install.sh https://gitlab.com/schmulschubiak/lookwearebusy/-/raw/main/hardware/lookwearebusy_install.sh`
  - sudo bash lookwearebusy_install.sh
